# f2p

A peer to peer file transfer utility.

## Getting Started

The one binary acts as a server and client. See below examples:

### Client

```sh
./f2p -a "127.0.0.1:9191" -f /tmp/y -t "<token>"
```

### Server

```sh
./f2p -s -f ./tmp/fileToUpload.exe -a "127.0.0.1:9191"
```

## Notes

I have yet to add support for UPnP, so for now you'll need to do some port forwarding. 