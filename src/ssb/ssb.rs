use std::net::{TcpStream};
use std::io::prelude::*;
use std::io::*;
use std::str;
use std::thread::{sleep, sleep_ms};

pub struct SSB{}
pub struct SSBControlled{}

pub trait Writer{
    fn send(stream: &mut TcpStream, data: Vec<u8>) -> Result<()>;
    fn recv(stream: &mut TcpStream) -> Result<Vec<u8>>;
}

pub trait WriterControlled{
    fn send(stream: &mut TcpStream, data: Vec<u8>, uprate: usize) -> Result<()>;
    fn recv(stream: &mut TcpStream, downrate: usize) -> Result<Vec<u8>>;
}

impl WriterControlled for SSBControlled{
    fn send(stream: &mut TcpStream, data: Vec<u8>, uprate: usize) -> Result<()>{
        // Initial write of 'size;' can be unthrottled 
        stream.write_all(data.len().to_string().as_ref())?;
        stream.write_all(";".as_ref())?;

        // Subsequent write of data package needs to be throttled.
        let chunks = data.chunks(uprate);
        for chunk in chunks{
            stream.write_all(chunk)?;
            sleep_ms(1000);
        };
        Ok(())
    }
    fn recv(stream: &mut TcpStream, downrate: usize) -> Result<Vec<u8>>{
        panic!();
    }
}

impl Writer for SSB{
    fn send(stream: &mut TcpStream, data: Vec<u8>) -> Result<()>{
        stream.write_all(data.len().to_string().as_ref())?;
        stream.write_all(";".as_ref())?;
        
        match stream.write_all(&data){
            Ok(_) => Ok(()),
            Err(e) =>{
                Err(e)
            }
        }
    }

    fn recv(stream: &mut TcpStream) -> Result<Vec<u8>>{
        let mut reader = BufReader::new(stream);
        let mut buffer: Vec<u8> = Vec::with_capacity(4096);
        reader.read_until(';' as u8, &mut buffer)?;
        buffer.pop();

        let bytes_to_read = str::from_utf8(&buffer).unwrap().parse::<usize>().unwrap();
        let mut buffer: Vec<u8> = vec![0; bytes_to_read];

        reader.read_exact(&mut buffer)?;
        Ok(buffer)
    }
}