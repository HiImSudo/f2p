use std::fs;

extern crate igd;
use igd::{SearchOptions};

extern crate rand;
use rand::prelude::*;
use rand::distributions::{Alphanumeric};

#[macro_use]
extern crate clap;
use clap::App;

use std::iter;

mod f2p;
use f2p::core::{Catalog, getPublicIp, portForward};

mod ssb;
use ssb::ssb::{Writer, SSB, SSBControlled, WriterControlled};

use std::net::{TcpListener, TcpStream};
use std::io::Error;


fn generateToken(size: u16) -> String{
    let mut rng = rand::thread_rng();
    let token: String = iter::repeat(()).map(|()| rng.sample(Alphanumeric))
        .take(size as usize)
        .collect();

    return token;
}

fn main() {
    let yaml = load_yaml!("arguments.yml");
    let matches = App::from_yaml(yaml).get_matches();
    let serve = matches.is_present("serve");
    let addr = matches.value_of("address").unwrap_or("127.0.0.1:9685");
    let filename = matches.value_of("file");
    let upnp = !matches.is_present("no-upnp");
    let autogenerate = matches.is_present("auto-generate");
    let uprate: Option<&str> = matches.value_of("uprate");
    let downrate: Option<&str> = matches.value_of("downrate");

    if serve{
        filename.expect("Please specify a file with the -f flag.");
        let persist = matches.is_present("persist");
        let uprate = match uprate{
            Some(n) => {Some(n.parse::<usize>().unwrap())},
            None => {None},
        };
        println!("{:?}", uprate);
        serve_file(filename.unwrap(), addr, persist, upnp, autogenerate, uprate);
    }else{
        let token = matches.value_of("token").unwrap();
        retrieve_file(filename.unwrap(), addr, token);
    }
}

fn serve_file(filename: &str, addr: &str, persist: bool, upnp: bool, autogenerate: bool, uprate: Option<usize>){
    let listener = TcpListener::bind(addr).unwrap();
    let data = fs::read(filename).unwrap();
    let token = generateToken(64);
    let public_ip = getPublicIp();
    let mut i_index = 0;
    let port = match addr.find(":"){
        Some(index) => {
            i_index = index;
            (&addr[index+1..addr.len()]).clone().parse::<u16>().unwrap()
        },
        None => {panic!("Invalid ip:port form")}
    };

    if !autogenerate{
        println!("Token: {}", token);
        println!("Private ip: {}", addr);
        println!("Public ip: {}", public_ip);
    } else {
        println!("Personal Network: -f out.file -a {} -t {}", addr, token);
        println!("Internet: -f out.file -a \"{}:{}\" -t {}", public_ip, port, token);
    }

    
    if upnp{
            println!("Forwarding {} and {}", addr[i_index + 1..addr.len()].parse::<u16>().unwrap(), &addr[0..i_index]);
            portForward(addr[i_index + 1..addr.len()].parse::<u16>().unwrap(), &addr[0..i_index]);
    }

    for res in listener.incoming(){
        match res{
            Ok(mut stream) => {
                if String::from_utf8_lossy(SSB::recv(&mut stream).unwrap().as_ref()) == token{
                    match uprate{
                        Some(rate) => {
                            SSBControlled::send(&mut stream, data.clone(), rate).expect("Something went wrong while sending the file");
                        },
                        None => {
                            SSB::send(&mut stream, data.clone()).expect("Something went wrong while sending the file");
                        }
                    }
                }
                if !persist{
                    break;
                }
            },
            Err(e) => {
                if !persist{
                    panic!(e);
                }else{
                    println!("Error {:?}", e);
                }
            }  
        }
    }
}

fn retrieve_file(filename: &str, addr: &str, token: &str){
    let mut stream = TcpStream::connect(addr).unwrap();
    match SSB::send(&mut stream, String::from(token).into_bytes()){
        Ok(()) => {},
        Err(e) => {
            panic!(e);
        }
    };

    match SSB::recv(&mut stream){
        Ok(data) => {
            fs::write(filename, data).unwrap();
        },
        Err(e) => {
            println!("{:?}", e);
            panic!();
        }
    };
}