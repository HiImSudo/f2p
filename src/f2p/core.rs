extern crate reqwest;
extern crate igd;
use std::fs;
use std::collections::HashMap;
use std::net::{SocketAddrV4, Ipv4Addr};
use std::error::Error;

#[derive(Debug)]
pub struct Catalog{
    filename: String,
    parts: Vec<PartInfo>,
    metadata: Option<String>
}

#[derive(Debug)]
pub struct PartInfo{
    start: usize,
    size: usize,
    // hash: String    
}


impl PartInfo{
    pub fn new(start: usize, size: usize) -> PartInfo{
        PartInfo{
            start,
            size,
            // hash: String::from("Replace me later")
        }
    }
}



impl Catalog{
    // ** We don't need the file to be read at this point, we only really need the file size.
    pub fn from_file(filename: &str) -> Catalog{
        let data = fs::read(filename).unwrap();
        let size = data.len();
        let block_size: f64 = (data.len() * 10/100) as f64;

        let block_count = (size as f64/block_size).ceil() as usize;
        let mut parts: Vec<PartInfo> = Vec::with_capacity(block_count);
        let mut start = 0;
        for _ in 0..block_count - 1 {
            parts.push(PartInfo::new(start, block_size as usize));
            start += block_size as usize;
        }

        if size % block_size as usize != 0{
            parts.push(PartInfo::new(start + block_size as usize, size % block_size as usize));
        }

        Catalog{
            filename: String::from(filename),
            parts,
            metadata: None
        }
    }
}

pub fn getPublicIp() -> String{
    let url = "https://ipecho.net/plain";
    reqwest::get(url).unwrap().text().unwrap()
}

pub fn portForward(port: u16, ip: &str){
    match igd::search_gateway(Default::default()){
        Err(e) => {
            println!("Error: {:?}", e.cause());
            panic!();
        },
        Ok(gateway) => {
            let localAddr = SocketAddrV4::new(ip.parse::<Ipv4Addr>().unwrap(), port);

            match gateway.add_port(igd::PortMappingProtocol::TCP, port, localAddr, 60 * 60 * 20, "f2p port forward"){
                Ok(_) => {
                    println!("Forwarded!");
                },
                Err(e) => {
                    panic!(e);
                }
            }
        }
    }
}